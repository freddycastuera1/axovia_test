import React from 'react'
import { Card } from './Card'

export const CardContanier = ({cakes}) => {
    console.log(cakes)
    return (
        <div className='container bg-pink-3 pt-2'>
            {
                cakes.map((cardInfo,key)=>(
                    <Card key={key} cake={cardInfo} />
                ))
            }
            
        </div>
    )
}