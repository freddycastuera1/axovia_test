import React from 'react'

export const Footer = () => {
    return (
        <footer className='footer bg-pink'>
            <div className='d-flex justify-content-between align-items-center nav'>
                <p className='mb-0 text-white'>ENG|</p>
                <ul className=" ms-auto mb-2 mb-lg-0 d-flex footer-list">
                    <li className="nav-item">
                        <a className="nav-link text-white" href="#">Blog</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link text-white" href="#">Contacto</a>
                    </li>

                </ul>
            </div>
        </footer>
    )
}
