import React from 'react'

export const Select = ({title,name,options,element:Element,value,onChange}) => {
	
	return (
		<div className='w-25 position-relative mx-1'>
			<div className='position-absolute top-25 start-7 '>
				<Element />
			</div>
			<select 
				className="form-select form-select-lg w-100 input border-round" 
				aria-label=".form-select-lg example" 
			
				value={value}
				onChange={onChange}
				>
				<option value={value} hidden>{value}</option>
				{
					options.map((option,key)=>(
						<option key={key} value={option}>{option}</option>
					))
				}
			</select>
		</div>

	)
}
