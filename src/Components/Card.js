import React from 'react'
const sizes = {
	'L':'Grande',
	'S':'Chico',
	'M':'Mediano'
}
const toppings = ['Fondeau','Betun italiano','Chantilly']

const formatPrice = (price) => '$'+parseFloat(price).toFixed(2)

const capitalize = (name) => name.slice(0,1).toUpperCase()+name.slice(1).toLowerCase()


export const Card = ({cake}) => {
	console.log(cake)
	const {name,imageUrl,description,price,comboPrice,toppingType,size} = cake
    // const name = 'pastel generico'
    // const imageUrl = 'https://cdn7.kiwilimon.com/brightcove/7034/7034.jpg'
    // const description = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium debitis provident molestias necessitatibus rerum officia, earum veniam sit fuga iste vel molestiae. Tenetur vero, consectetur laboriosam aspernatur alias dolorum totam.'
    // const price = '500.00000'
    // const comboPrice = '1000.0000'
    // const toppingType = 0
    // const size = 'S'
  return (
		<div className='my-4 bg-white card border-round'>
    <div className='container  '>
      <div className='row'>
				<div className='col-12 col-xl-8 border-bottom d-flex align-items-center justify-content-center justify-content-md-start'>
					<span className='fw-bold fs-5'>{capitalize(name)}</span>
				</div>
				<div className='d-none d-xl-block col-2 bg-red px-1 py-3 text-uppercase text-white banner'>pastel</div>
				<div className='d-none d-xl-block col-2 bg-orange px-1 py-3 text-uppercase text-white banner paquete'>paquete</div>
      </div>      
			<div className='row '>
				<div className='col-12 col-xl-8 d-flex flex-column flex-md-row'>
					
					<img  
						src={imageUrl}
						className = 'image'
						
					/>
					
					<div className='d-flex flex-column align-items-start ms-3 '>
						<p className='text-start mt-3'>{description}</p>
						<div className='d-flex flex-column align-items-start mt-3'>
							<p><span className='fw-bold'>Tamaño: </span>{sizes[size]}</p>
							<p className='topping'><span className='fw-bold'>Topping: </span>{toppings[toppingType]}</p>
						</div>
						
					</div>
					<div>

					</div>
				</div>

				<div className='d-none d-xl-flex col-2  flex-column justify-content-around'>
					<p className='fw-bold fs-6 mt-2'>15-17 personas</p>
					<p className='fw-bold mb-0 price'>{formatPrice(price)}</p>
					<span className='text-muted mb-4'>*solo pastel</span>
					<button className='btn btn-blue rounded-pill mb-5 button '>seleccionar</button>
				</div>
				<div className='d-none d-xl-flex col-2 d-flex flex-column justify-content-around'>
					<p className='fw-bold mt-2'>15-17 personas</p>
					<p className='fw-bold mb-0 price'>{formatPrice(comboPrice)}</p>
					<span className='text-muted mb-4'>*Bebidas Incluidas</span>
					<button className='btn btn-blue rounded-pill mb-5 button-secondary'>seleccionar</button>
				</div>
			</div>      
		<div className='row'>
			<div className='d-block d-xl-none col-6 d-flex flex-column'>
					<div className='bg-red p-3'>pastel</div>
					<p className='fw-bold fs-6 mt-2'>15-17 personas</p>
					<p className='fs-1 fw-bold mb-0'>{formatPrice(price)}</p>
					<span className='text-muted mb-4'>*solo pastel</span>
					<button className='btn btn-blue rounded-pill mb-5 button'>seleccionar</button>
				</div>
				<div className='d-block d-xl-none col-6 d-flex flex-column'>
					<div className='bg-orange p-3'>paquete</div>
					<p className='fw-bold mt-2'>15-17 personas</p>
					<p className='fs-1 fw-bold mb-0'>{formatPrice(comboPrice)}</p>
					<span className='text-muted mb-4'>*Bebidas Incluidas</span>
					<button className='btn btn-blue rounded-pill mb-5 button-secondary'>seleccionar</button>
				</div>
		</div>
    </div>
		</div>
  )
}