import './App.css';
import {Select} from './Components/Select'
import {FaRulerCombined,FaDollarSign} from "react-icons/fa"
import { RiArrowUpDownFill } from "react-icons/ri";
import {GiCupcake} from 'react-icons/gi'
import { Nav } from './Components/Nav';
import data from './AxoviaData.json'
import { CardContanier } from './Components/CardContanier';
import {Filters} from './Components/Filters'
import {useField} from './hooks/useField'
import {useState,useEffect} from 'react'
import filterByPrice from './utilities/filterByPrice';
import filterByPriceCombo from './utilities/filterByPriceCombo';
import filterByTopping from './utilities/filterByTopping';
import filterBySize from './utilities/filterBySize';
import orderByPrice from './utilities/orderByPrice';
import { Search } from './Components/Search';
import filterBySearch from './utilities/filterBySearch'
import { Footer } from './Components/Footer';

function App() {
  console.table(data)
  const price = useField('Precio')
  const priceCombo = useField('Precio combo')
  const topping = useField('Topping')
  const size = useField('Tamano')
  const order = useField('Ordenar por')
  const search = useField('')

  const [filteredCakes,setFilteredCakes] = useState(data)
  useEffect(()=>{
    const filteredByPrice = filterByPrice(data,price.value)
    const filteredByPriceCombo = filterByPriceCombo(filteredByPrice,priceCombo.value)
    const filteredByTopping = filterByTopping(filteredByPriceCombo,topping.value)
    const filteredBySize = filterBySize(filteredByTopping,size.value)
    const orderedByPrice = orderByPrice(filteredBySize,order.value)
    const filteredBySearch = filterBySearch(orderedByPrice,search.value)
    setFilteredCakes(filteredBySearch)
  },[price.value,priceCombo.value,topping.value,size.value,order.value,search.value])

  return (
    <div className="App">
    <Footer/>
    <Nav />
    <Filters >
        <Search 
          {...search}
        />
        <Select 
          title='Precio' 
          options={['Hasta $299','$300 a $399','De $400 en adelante','Todos']} 
          element={FaDollarSign}
          {...price}
          />
        <Select 
          title='Precio Combo' 
          options={['Hasta $499','$500 a $999','De $1000 en adelante','Todos']} 
          element={FaDollarSign}
          {...priceCombo}
          />
        <Select 
          title='Topping' 
          name='topping'
          options={['Fondeau','Betun Italiano','Chantilly','Todos']} 
          element={GiCupcake}
          {...topping}
          />
        <Select 
          title='Tamaño' 
          name='size'
          options={['Pequeño','Mediano','Grande','Todos']} 
          element={FaRulerCombined}
          {...size}
          />
        <Select 
          title='Precio' 
          name='order'
          options={['Mas caro','Mas barato']} 
          element={RiArrowUpDownFill}
          {...order}
          />
      </Filters>
     
      <CardContanier  cakes={filteredCakes} />
    </div>
  );
}

export default App;
