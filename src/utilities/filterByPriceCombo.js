export default function filterByPriceCombo(arr,price){
    switch(price){
      case 'Hasta $499':
        return arr.filter(item=>Number(item.comboPrice)<=499)
      case '$500 a $999':
        return arr.filter(item=>Number(item.comboPrice)>=500 && Number(item.comboPrice)<=999)
      case 'De $1000 en adelante':
        return arr.filter(item=>Number(item.comboPrice)>=1000)
      default:
        return arr
    }
  }