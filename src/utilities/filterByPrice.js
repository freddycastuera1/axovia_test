export default function filterByPrice(arr,price){
    switch(price){
      case 'Hasta $299':
        return arr.filter(item=>Number(item.price)<=299)
      case '$300 a $399':
        return arr.filter(item=>Number(item.price)>=300 && Number(item.price)<=399)
      case 'De $400 en adelante':
        return arr.filter(item=>Number(item.price)>=400)
      default:
        return arr
    }
  }