export default function filterByTopping(arr,topping){
    switch(topping){
      case 'Fondeau':
        return arr.filter(item=>item.toppingType==="0")
      case 'Betun Italiano':
        return arr.filter(item=>item.toppingType==="1")
      case 'Chantilly':
        return arr.filter(item=>item.toppingType==="2")
      default:
        return arr
    }
  }