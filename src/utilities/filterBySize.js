export default function filterBySize(arr,size){
    switch(size){
      case 'Pequeño':
        return arr.filter(item=>item.size==="S")
      case 'Mediano':
        return arr.filter(item=>item.size==="M")
      case 'Grande':
        return arr.filter(item=>item.size==="L")
      default:
        return arr
    }
  }