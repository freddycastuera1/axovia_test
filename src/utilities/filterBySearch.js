export default function filterBySearch(arr,search){
    return search===''?arr:arr.filter(item=>item.name.toLowerCase().includes(search.toLowerCase()))
  }