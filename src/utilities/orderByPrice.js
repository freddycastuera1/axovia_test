export default function orderByPrice(arr,order){
    if(order==='Mas caro') return [...arr].sort((a,b)=>Number(b.price)-Number(a.price))
    if(order==='Mas barato') return [...arr].sort((a,b)=>Number(a.price)-Number(b.price))
    return arr
  }